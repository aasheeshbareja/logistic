const mongoose = require('mongoose');
const APIError = require('../helper/apiError');
const Schema = mongoose.Schema;
const constants = require('../util/constants');


const orderSchema = new Schema({
    id: String,
    distance: String,
    status: String,
    origin: Array,
    destination: Array
});

/** This function will be called before saving into db  **/

orderSchema.pre("save", async function (next)  {
    try {
        const duplicate = await orderModelSchema.findOne({
            id: this.id
        });
        if (duplicate) {
             next(new Error (
              "ORDER_ALREADY_EXISTS"
            ));
        } else {
            next();
        }
        } catch (err) {
        return next(new Error(err));
    }
})

/** This function is for updating the order  **/

orderSchema.statics.updateOrder = async function (orderId, updatedOrder)  {
    try {
        //check whether order exist or not
        let isOrderExist = await orderModelSchema.findOne({
            id: orderId
        });
        if (!isOrderExist) {
            throw new APIError(404, `No order found with ID ${orderId}`);
            }
            const order = await orderModelSchema.findOneAndUpdate({
            id : orderId,
             status: constants.orderStatus.UNASSIGNED
         }, {
             status: updatedOrder.status
         });
         if (!order) {
             throw new APIError(409, `ORDER_ALREADY_BEEN_TAKEN`);
         }
    } catch (err) {

        return Promise.reject(err);
    }
}

/** This method is to list the orders  **/

orderSchema.methods.getOrders = async function(query, fields, skip, limit, sequence=-1) {
    try {
        const orders = await orderModelSchema.find(query, fields)
            .skip(skip)
            .limit(limit)
            .sort({'id': sequence})
            .exec();
        if (!orders.length) {
            return [];
        }
        return orders.map(order => order.toObject());
    } catch (err) {
        return Promise.reject(err);
    }
}


/* Transform with .toObject to remove __v and _id from response */
if (!orderSchema.options.toObject) orderSchema.options.toObject = {};
orderSchema.options.toObject.transform = (doc, ret) => {
    const transformed = ret;
    delete transformed._id;
    delete transformed.__v;
    delete transformed.origin;
    delete transformed.destination;
    return transformed;
};
const orderModelSchema = mongoose.model('Order', orderSchema);
module.exports = orderModelSchema;
