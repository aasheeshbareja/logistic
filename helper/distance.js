let googleDistance = require('google-distance');
const config = require('../util/config-util').nodeConfig;
const APIError = require('../helper/apiError');
googleDistance.apiKey = config.get('GOOGLE_KEY');

module.exports = {

    /** This is for calculating the distance between two set of coordinates **/

    async getDistance(origin, destination) {
        return new Promise((resolve, reject) => {
            googleDistance.get({
                    index: 1,
                    origin: origin,
                    destination: destination
                },
                (err, data) => {
                  if (err) {
                        reject(err);
                        }
                    resolve(data ? data.distanceValue :  undefined);
                });
        });
    }
}