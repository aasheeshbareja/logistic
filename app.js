//var createError = require('http-errors');
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const APIError = require('./helper/apiError');
const url = 'mongodb://mongodb/logistic';
mongoose.connect(url);
const order = require('./routes/order');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/orders', order);

app.get('*', function(req, res, next) {
    return next(
        new APIError(
            404,
            `${req.path} is not valid path.`
        )
    );
})

app.all('*', function(req, res, next) {
    return next(
        new APIError(
            405,
            `Method Not Allowed,
            ${req.method} method is not supported at ${req.path}.`
        )
    );
})

app.use(function(err, req, res, next) {
    let error = err;
    if (!(error instanceof APIError)) {
        error = new APIError(500, err.message);
    }
    return res.status(error.status).json(error);
})
module.exports = app;
