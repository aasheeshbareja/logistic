var nconf = require('nconf');
var environment = process.env.NODE_ENV || 'development';


function nodeConfig() {
    nconf.argv()
        .env()
        .file({ file: './config/'+environment.toLowerCase() + '.json' });
}

nodeConfig.prototype.get = function(key) {
    return nconf.get(key);
};

module.exports.nodeConfig = new nodeConfig();
