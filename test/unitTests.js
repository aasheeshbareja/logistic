const APIError = require('../helper/apiError');
const pagelimit = require('../helper/pagelimit');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const googleDistance = require('google-distance');
const googleDistanceResponse = {
    "index":1,
    "distance":"90.2 km",
    "distanceValue":90150,
    "duration":"2 hours 4 mins",
    "durationValue":7435,
    "origin":"13, Maruti Udyog, Sector 18, Gurugram, Haryana 122016, India",
    "destination":"Mahaveer Marg, Firozpur Jhirka, Haryana 122104, India",
    "mode":"driving","units":"metric","language":"en",
    "avoid":null,
    "sensor":false
}
const distanceUtils = require("../helper/distance");


describe('Parse page limit helper function', () => {
    describe('parsePageLimit()', () => {
        it('should return a number if a valid limit is passed', () => {
            const limit = '2';
            const validatedLimit = pagelimit.pageLimit(limit);
            expect(validatedLimit).equal(2);
        });
        it('should return an API Error if a non-numeric limit is passed', () => {
            const limit = 'foo';
            const validatedLimit = pagelimit.pageLimit(limit);
            expect(validatedLimit).to.be.an.instanceof(APIError);
        });
        it('should return an API Error if zero is passed when type is "limit"', () => {
            const limit = '0';
            const validatedLimit = pagelimit.pageLimit(limit);
            expect(validatedLimit).to.be.an.instanceof(APIError);
        });
        it('should return an API Error if a negative limit is passed', () => {
            const limit = '-1';
            const validatedLimit = pagelimit.pageLimit(limit);
            expect(validatedLimit).to.be.an.instanceof(APIError);
        });
        it('should return a number if a valid numeric limit is passed', () => {
            const limit = '3';
            const validatedLimit = pagelimit.pageLimit(limit);
            expect(validatedLimit).equal(3);
        });
    });
});

/**  Google distance Test case  **/
describe('Google Distance Test', () => {
    describe('finding distance', () => {
        it('should return distance between origin and destination', (done) => {
            const distanceStub = sinon
                .stub(googleDistance, 'get')
                .yields(null, googleDistanceResponse);

            distanceUtils.getDistance({origin:["28.491178", "77.074327"], destination:["27.787343", "76.946463"]})
                .then((data) => {
                    distanceStub.restore()
                    expect(data).to.equal(90150);
                    done();
                });
        });

        it('should return an error if coordinates are wrong', (done) => {
            const distanceStub = sinon
                .stub(googleDistance, 'get')
                .yields({message: 'Result error: ZERO_RESULTS'}, null);

            distanceUtils.getDistance({origin:["2", "72"], destination:["28", "79"]})
                .then(null, (err) => {
                    distanceStub.restore();
                    expect(err.message).equal('Result error: ZERO_RESULTS');
                    done();
                });
        });
    });
});


describe('APIError module', () => {
    describe('APIError', () => {
        it('error reponse contain status and message property', () => {
            const response = new APIError(409, `ORDER_ALREADY_BEEN_TAKEN`);
            expect(response).to.have.property('status');
            expect(response).to.have.property('message');
        });
    });
});
