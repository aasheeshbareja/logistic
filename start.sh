# Checking npm
if [ ! -f /usr/bin/npm ]; then
    echo 'Installing npm'
    # Install npm from the Debian-based distributions repository
    sudo apt-get install -y npm

else
    echo "npm already installed.  Skipping.."
fi

# # Start the application
sudo npm install 
sudo docker-compose up -d
echo 'Installing Dependencies'
sleep 150
## Start Test Cases
echo 'Starting Test Suite'
echo 'Running Unit Tests'
sudo docker exec logistic_app_1 npm test test/unitTests
echo 'Running Integration Tests'
sudo docker exec logistic_app_1 npm test test/integrationTests
