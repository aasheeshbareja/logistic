const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order');

router.post('/', orderController.createOrder);
router.patch('/:id', orderController.updateOrder);
router.get('/', orderController.getOrder);

module.exports = router;
