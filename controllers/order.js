const OrderModel = require("../models/order");
const distanceApi = require("../helper/distance");
const updateOrderSchema = require("../schemas/updateorder");
const orderNewSchema = require("../schemas/neworder");
const pagelimit = require('../helper/pagelimit');
const mongoose = require('mongoose');
const APIError = require('../helper/apiError');
const constants = require('../util/constants');
const {
    validate
} = require('jsonschema');

module.exports = {
    /** This is for creating new order **/

    createOrder: async function (req, res, next) {
        try {
            const validation = validate(req.body, orderNewSchema);
            if (!validation.valid) {
                return next(
                    new APIError(
                        400,
                        "INVALID_PARAMETERS"
                    )
                );
            }
            let data = req.body;
            const origin = data.origin.toString();
            const destination = data.destination.toString();
            let totalDistance = await distanceApi.getDistance(origin, destination);
            const orderData = {
                id: mongoose.Types.ObjectId(),
                distance: totalDistance,
                status: constants.orderStatus.UNASSIGNED,
                origin: data.origin,
                destination: data.destination
            }
            let newOrder = await new OrderModel(orderData).save();
            return res.status(200).json({
                id: newOrder.id,
                distance: parseInt(newOrder.distance),
                status: newOrder.status
            });
        } catch (e) {
            if (e === "Error: Result error: ZERO_RESULTS") {
                res.send({error: "INVALID_PARAMETERS"})
            } else {
                res.send({error: e.toString()});
            }

        }
    },

    /** This is for updating the order status with given order id **/

    updateOrder: async function (req, res, next) {
        let id = req.params.id;
        const validation = validate(req.body, updateOrderSchema);
        if (!validation.valid) {
            return next(
                new APIError(
                    400,
                    "INVALID_PARAMETERS"
                )
            );
        }

        try {
            const order = await OrderModel.updateOrder(id, req.body);
            return res.status(200).json({
                status: "SUCCESS"
            });
        } catch (err) {
            next(err);
        }

    },

    /** This is for orders listing, we can customize how much orders we want to list by giving parameters  **/

    getOrder: async function (req, res, next) {
        let limit = pagelimit.pageLimit(req.query.limit, 'limit');
        let page = pagelimit.pageLimit(req.query.page, 'page');
        let skip = limit * (page - 1) || 0;

        if (typeof page !== 'number') {
            return next(page);
        } else if (typeof limit !== 'number') {
            return next(limit);
        }
        try {
            const orders = await new OrderModel().getOrders({}, {}, skip, limit);
            return res.json(orders);
        } catch (err) {
            next(err);
        }
    }
}