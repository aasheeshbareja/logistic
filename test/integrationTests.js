const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);
const constants = require('../util/constants');
const mongoose = require('mongoose');
const server = 'localhost:8080';

/** Test DB connection  **/
 describe('Test Suite', function() {
   before(function (done) {
    mongoose.connect('mongodb://localhost/logistic', function (error) {
      if (error) {}
      done(error);
    });
  });
});

/** No route found test case  **/
describe('GET /', () => {
  it('should return 404 for urls which are not defined', (done) => {
    chai.request(server)
      .get('/')
      .end(function(err, res) {
        expect(res).to.have.status(404);
        done();
      });
  });
});

/** Create order test case  **/
describe('/POST orders method', () => {

    it('should return failure when updating order status to TAKEN', (done) => {
        chai.request(server)
            .post('/orders')
            .send({
                origin: ["28.58484", "77.111761"],
                destination: ["28.530264", "77.111761"]
            })
            .end((err, res) => {
                var order_id = JSON.parse(res.text).id;
                chai.request(server)
                    .patch('/orders/' + JSON.parse(res.text).id)
                    .send({
                        status: constants.orderStatus.TAKEN
                    }).end((err, res) => {

                        chai.request(server)
                        .patch('/orders/' + order_id)
                        .send({
                            status: constants.orderStatus.TAKEN
                        }).end((err, res) => {
                        expect(res).to.have.status(409);
                        chai.request(server)
                            .patch('/orders/' + order_id)
                            .send({
                                status: constants.orderStatus.UNASSIGNED
                            }).end((err, res) => {
                            done();
                        });
                    });
                })
            });


    });
    it('should return 400 when origin & destination parameters are incorrect',
        (done)=>{
            chai.request(server)
                .post('/orders')
                .send({
                    origin: ["28.93", "", "43.2345"],
                    destination: ["34.322323", "57"]
                })
                .end((err, res)=>{
                    expect(res).to.have.status(400);
                    done();
                });
        });


    it('should return 400 with if format is invalid', (done) => {
        chai.request(server)
            .post('/orders')
            .send({
                origin: [28, "72.111761"],
                destination: ["24.530264", "73.8789"]
            })
            .end((err, res) => {
                expect(res).to.have.status(400);
                done();
            });
    });

    it('should create order with valid parameters', (done) => {
        chai.request(server)
            .post('/orders')
            .send({
                origin: ["28.58484", "77.111761"],
                destination: ["28.530264", "77.111761"]
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('distance');
                expect(res.body.distance).to.be.a('number');
                done();
                data = res;
            });
    });

    it('should return response status UNASSIGNED for new order', (done) => {
        chai.request(server)
            .post('/orders')
            .send({
                origin: ["28.530264", "77.111761"],
                destination: ["28.530264", "77.111761"]
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.status).to.be.equal(constants.orderStatus.UNASSIGNED);
                done();
            });
    });
});

/** order update test case  **/

describe('/PATCH /orders/:id', () => {

    it('should update the order with correct status', (done) => {
        chai.request(server)
            .post('/orders')
            .send({
                origin: ["28.58484", "77.111761"],
                destination: ["28.530264", "77.111761"]
            })
            .end((err, res) => {
                chai.request(server)
                    .patch('/orders/' + res.body.id)
                    .send({
                        status: constants.orderStatus.TAKEN
                    })
                    .end((err, res) => {
                        expect(res).to.have.status(200);
                        done();
                    });
                });
    });
  it('should return 404 if order not found with given order id', (done) => {
    chai.request(server)
      .patch('/orders/5bc07c1c478e1313f08333bb')
      .send({
        status: constants.orderStatus.TAKEN
      })
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      });
  });
    it('should return 400 when status value is not correct',
        (done)=>{
            chai.request(server)
                .patch('/orders/5bc07c1c478e1313f08333bb')
                .send({
                    status: 100
                })
                .end((err, res)=>{
                    expect(res).to.have.status(400);
                    done();
                });
        });

  it('should return 400 for bad format', (done) => {
    chai.request(server)
      .get('/orders?page=1&limit=1')
      .end((err, res) => {
        let orderResponse = res;
        chai.request(server)
          .patch('/orders/' + res.body[0].id)
          .send({
            position: constants.orderStatus.TAKEN //Wrong format as param not supported
          }).end((err, res) => {
            expect(res).to.have.status(400);
            done();
          })
      });
  });
});



/** order listing test case  **/

describe('GET /', () => {
  it('should return maximum two orders (limit=2)', (done) => {
    chai.request(server)
      .get('/orders?page=1&limit=2')
      .end(function(err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });

  it('should return wrong limit data type error with (limit=xyz)', (done) => {
    chai.request(server)
      .get('/orders?page=1&limit=xyz')
      .end(function(err, res) {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return wrong page datatype error with (page=suv)', (done) => {
    chai.request(server)
      .get('/orders?page=suv&limit=1')
      .end(function(err, res) {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return error with negative limit value', (done) => {
    chai.request(server)
      .get('/orders?page=abc&limit=-1')
      .end(function(err, res) {
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should return error with page value 0', (done) => {
    chai.request(server)
      .get('/orders?page=0&limit=1')
      .end(function(err, res) {
        expect(res).to.have.status(400);
        done();
      });
  });
});
