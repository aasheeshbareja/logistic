const APIError = require('./apiError');

module.exports = {

    /** This is for testing the valid values of page and limit for get orders  **/

    pageLimit(val, type = 'limit') {
        if (!val) {
        return null;
    }
    const num = +val;
    const min = 1;

    if (!Number.isInteger(num)) {
        return new APIError(
            400,
            `Invalid ${type}: '${val}', ${type} needs to be an integer.`
        );
    } else if (num < min) {
        return new APIError(
            400,
            `limit & page should be equal or greater than ${min}`
        );
    }
    return num;
}
}